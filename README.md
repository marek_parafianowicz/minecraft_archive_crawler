# Minecraft Sales Crawler

Minecraft Sales Crawler helped me collect data about Minecraft sales (daily and total) for my master's degree paper. Crawler uses [Mechanize](http://mechanize.rubyforge.org/) and [Nokogiri](http://www.nokogiri.org/) to fetch sales data from Minecraft WebArchive - https://web.archive.org/web/20160428200736/https://minecraft.net/stats. It is crawling back into the past of the archive.

## Prerequisites

Application requires Ruby language.

For Ruby installation instruction visit [official ruby language website](https://www.ruby-lang.org/en/).

## Installation

```bash
$ git clone git@bitbucket.org:marek_parafianowicz/minecraft_archive_crawler.git
$ cd MinecraftStats
```

## Running

`$ ruby minecraft_sales_crawler.rb`

Application has it's default behaviour set in minecraft_sales_crawler.rb. I find it more convinient than typing all the data in console.

Data is saved in CSV file which is set in crawlers `initialize` method.

```ruby
def initialize(file)
	@file = file
end
```

Main public method `crawl` argument (`how_many_times`) sets how many times crawler should change the site.

```ruby
def crawl(how_many_times)
	#
end
```

Crawler changes sites in random time intervals set in `crawl` method.

```ruby
def crawl(how_many_times)
	#
	delay_time = rand(15)
	sleep(delay_time)
	#
end
```