require 'mechanize'
require 'csv'


class Crawler
	def initialize(file)
		@file = file
	end

	def crawl(how_many_times)
		setup_agent
		page = @agent.get('https://web.archive.org/web/20160428200736/https://minecraft.net/stats')
		html(page)

		how_many_times.times do 
			print get_date("Day") + " " + get_date("Month") + " " + get_date("Year")
			sales = @html.xpath("//p").text.scan(/\d/)
			check_sales(sales)
			save_CSV
			delay_time = rand(15)
			sleep(delay_time)
			check_page
		end
	end

private
	# Setup agent
	def setup_agent
		@agent = Mechanize.new { |agent|
	  	agent.user_agent_alias = 'Mac Safari'
		}
	end

	# Get HTML using Nokogiri
	def html(page)
		@html = Nokogiri::HTML(page.body)
	end

	# Extract date from HTML of archive
	def get_date(kind)
		# Arguments shuld be restricted to Day, Month and Year
		@html.css("#display#{kind}El").text
	end

	# Check if there is an information about sales. If so, prepare them to save and display info in terminal
	def check_sales(sales)
		if sales.empty?
			puts "No info about sales."
		else
			2.times { sales.delete_at(8) }
			sales = sales.join('')
			@whole_sales = sales[0..7]
			@daily_sales = sales[8..-1]
			puts " Whole sales: " + @whole_sales + ". Last 24h sales: " + @daily_sales + "."
		end
	end

	# Check if the page was correctly read (avoids web archive errors)
	def check_page
		old_sum = get_date("Day") + get_date("Month")
		page = @agent.page.link_with(text: /Previous capture/).click
		html(page)
		sum = get_date("Day") + get_date("Month")
		if sum == old_sum
			page = @agent.page.links[1].click
			html(page)
		end
	end

	# Save data in CSV file
	def save_CSV
		CSV.open(@file, 'a+') do |csv|
			csv << [get_date("Day"),  get_date("Month"), get_date("Year"), @whole_sales, @daily_sales]
		end
	end
end

crawlerek = Crawler.new('minecraft_sales_statistics.csv')
crawlerek.crawl(10)